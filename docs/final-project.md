# Projet final

Cliquez [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maud.maes/-/raw/master/docs/images/ficheA4-Maud.pdf?inline=false) pour télécharger le PDF A4

## Chaise H   
La Chaise H est une chaise à bascule conçue sur mesure pour des enfants. Tournez la chaise et elle se transforme en un marchepied. Le design de la Chaise H est simple et facile à assembler. Huit fentes permettent à quatre surfaces de glisser les unes dans les autres facilement. Pour transformer la chaise en marchepied, il suffit d'insérer la surface du siège et le dossier dans les quatre fentes restantes et retourner l'ensemble. L'idée derrière la Chaise H est la même que celle du H-Horse du designer Oki Sato. Une poutre en H est transformée d'un élément structurel en une utilisation différente avec un sens de l'enjouement. La conception est née de la recherche de connexions entre les surfaces de bois sans avoir à utiliser de matériau supplémentaire pour les fixer ensemble. La Chaise H est réalisée en bois mais le design permet également de la réaliser en plastique.

![](images/0final/stoel3frames.gif)  

![](images/0final/DSCF2815.jpeg)  

![](images/0final/schommel.jpeg)  

### Caractéristiques  
* Dimensions: 400x400x400 mm  
* Poids: 3,5 kg  
* Matériau: contreplaqué de bouleau 12 mm  
* hauteur maximale du siège: 200 mm  
* hauteur maximale du marchepied: 257 mm  
* age: 3 à 5 ans  

![](images/0final/DSCF2760.jpeg)  

![](images/0final/details.jpg)  

![](images/0final/opbouw.jpg)    

![](images/0final/DSCF2866.jpeg)    

<iframe src="https://myhub.autodesk360.com/ue2d88596/shares/public/SH56a43QTfd62c1cd968e1580d99b59af3e9?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>
<br/>   

Cliquez [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maud.maes/-/raw/master/docs/images/surfaceAssise.svg?inline=false) pour télécharger le fichier technique de la surface assise.  
![](images/surfaceAssise.svg)  
Cliquez [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maud.maes/-/raw/master/docs/images/surfaceDossier.svg?inline=false) pour télécharger le fichier technique de la surface dossier.  
![](images/surfaceDossier.svg)  
Cliquez [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maud.maes/-/raw/master/docs/images/surfaceLaterale.svg?inline=false) pour télécharger le fichier technique de la surface laterale.  
![](images/surfaceLaterale.svg)  


## Recherches

### Analyse de l'objet choisi
#### H-Horse  

Le cheval à bascule minimaliste a été conçu par Oki Sato, le chef concepteur du bureau de design japonais Nendo. Le design a été commandé par la célèbre maison de design italien Kartell.
Au contraire d’un cheval à bascule classique en bois, ce cheval à bascule est réalisé en polycarbonate transparent. H-Horse mesure 61 centimètres de large, 72 centimètres de haut, 27,5 centimètres de profondeur et est disponible en différentes couleurs. L’objet pèse un peu moins de 5 kg.

Les trois parties transparentes courbées qui composent le cheval à bascule se réfèrent aux poutres en H en acier utilisé dans les grandes structures architecturale comme des bâtiments et des ponts. C’est la section en H des poutres en acier qui assure la résistance mécanique. En appliquant ce concept directement au cheval à bascule d’un enfant, Oki Sato a créé une forme qui présente à la fois une fonction et une résistance avec un minimum de matériaux. La conversion de la fonction d’un élément structurel en une nouvelle fonction plus ludique est conforme à l’histoire de la société Kartell. L’entreprise de meubles pour laquelle Oki Sato a conçu H-Horse a commencé à produire et à vendre des équipements de laboratoire très fonctionnels. Ils ont finalement évolué vers une production de meubles et d’accessoires en plastique qui sont admirés dans le monde entier.  
![](images/sketch.jpg)  
![](images/horse06.jpg)

Sources:  
[Site web de Nendo](http://www.nendo.jp/en/works/h-horse-2/), [site web de Kartel](https://www.kartell.com/CH/nendo/h-horse/03270), [Dezeen](https://www.dezeen.com/2016/04/06/nendo-transparent-h-rocking-horse-toy-kartell-milan-2016/)

### Définir une manière d'approche
Le début de l'exercice: choisissez parmi une liste de 5 mots 1 mot qui correspond le mieux à votre façon de penser, de travailler, de concevoir...  

- Référence  
- Influence  
- Hommage  
- **inspiration**  
- Extension  

Après avoir pesé et comparé les différents mots je pense que le mot 'inspiration' est le plus proche de ma façon de travailler et de concevoir. J'ai rassemblé ci-dessous les définitions qui me conviennent le mieux et que je trouve le plus appropriées pour le mot 'inspiration' dans le contexte de l'exercice.  

INSPIRATION:  

- Acte de stimulation de l’intellect, des émotions et de la créativité à partir d’une influence. (Wiktionnary)  
- Idée, résolution spontanée, soudaine. Une heureuse inspiration. (Le Petit Robert)  
- Enthousiasme, souffle créateur qui anime l'écrivain, l'artiste, le chercheur. (Larousse)  
- L'enthousiasme qui entraîne les poètes, les musiciens, les peintres. (Littré)  

Ce que je trouve les belles définitions sont celle où l'inspiration est exprimée sous forme d'enthousiasme. J'aime l'ambiance positive et passionnée qu'elle donne au mot 'inspiration'.  

Ma définition personnelle de l'inspiration est: observer et comparer des œuvres (d'art), des objets, des dessins, des photos existantes... ce qui donne envie de se mettre soi-même au travail de manière créative.  

Dans un processus de conception, je travaille souvent de la manière suivant: je dessine beaucoup, j'étudie des projets similaires, je fais des croquis, je cherche des projets de référence, je regarde des photos, des modèles, des plans,... J'en tire une inspiration sur laquelle je continue à travailler et avec laquelle j'obtiens finalement le propre design. J'aurais aussi pu choisir le terme 'référence' puisque, dans le cadre de mes recherches sur le design, je fais également des recherches sur des project de référence. Mais je pensai que le mot était trop spécifique. J'essaie de chercher bien plus que des exemples de référence et je ne m'en inspire pas toujours.

En ce moment, je réfléchis à deux approches possibles. Comme l'objet du musée que j'ai choisi est un cheval à bascule pour enfants, ma première approche est de continuer à travailler sur le concept de 'cheval à bascule'. Je pourrais inventer une nouvelle forme, travailler avec un matériau différent, réfléchir davantage à ce qu'est un cheval à bascule et pousser la définition à ses limites.  
Ma deuxième approche est de penser comme le concepteur Oki Sato, à une conception avec un poutre en H comme base ou un autre élément structurel. Comment transformer un poutre en H en un nouvel objet ayant une signification complètement différente?  
Je commencerai par faire des croquis pour les deux approches et je verrai quelle idée a lé plus à offrir à la fin.  
Vous pouvez lire plus d'informations sur l'objet que j'ai choisi ci-dessous et dans la partie de l'index.

Voici quelques exemples de projets existants qui correspondent à mes deux approches. Le premier cheval à bascule a été conçu par Michael Svane Knap, le deuxième par Nipa Doshi et le troisième par Federal Design House. Les trois dessins sont tous une réinterprétation inspirante d'un cheval à bascule. Le quatrième projet est le '[H Beam Lamp](https://joohoyoung.com/H-beam-lamps)', conçu par Joo Ho Young. Ce projet correspond bien à ma deuxième approche de transformer un élément structurel à un objet ayant une fonction totalement différente.  

![](images/inspiration.jpg)  

### Premier essaie  
(semaine du 26/11/20)

#### Qu'est-ce que c'est un poutre en H?  
Un type de poutrelle en acier de construction, laminée à chaud, où en coupe transversale, on voit un H.  
Outre les profils avec un H en section transversale, il existe également d'autres formes telles que I, U, O et T. Les poutres en H peuvent également être subdivisés en différents types:  
- **profils HEA**: avec le profil HEB, c'est l'espèce la plus commune. Reconnaissables à une bride légèrement plus mince. Avec les poutres HEA, la hauteur de la poutre est inférieure à la largeur de la bride.  
- **profils HEB**: par rapport à HEA, la poutre HEB a une plus grande capacité portante et est légèrement plus lourde et plus large que la HEA.  
- **profils HEM**: la version la plus robuste et la plus lourde des poutrelles en acier à en H. La poutre HEM est plus grosse, plus lourde et la bride est plus épaisse pour des charges plus lourdes.  
- **profils HL**: a des brides plus larges que les profils HEA et HEB.   
- **profils HD**: a des brides et un corps plus épais, souvent utilisé comme colonne.  
- **Profils HP**: a des brides larges mais fines et une semelle mince, souvent utilisé pour des fondations.  

![](images/profils.jpg)  

#### 3 idées  
Pour ma première idée, je pars d'une poutre en H avec les proportions d'un cube. En y ajoutant un plateau horizontal, j'ai obtenu une chaise (voir dessin 1). J'ai dessiné ce modèle dans Fusion 360 pour pouvoir l'imprimer en 3D par la suite. Le résultat de l'impression à l'air bon, mais je n'étais pas encore satisfait de la forme. Pour l'instant, il s'agit d'une conception très anguleuse et rigide pour une chaise qui doit être confortable.  
J'en ai tiré une deuxième idée. J'ai fait pivoter la poutre en H pour que la semelle soit horizontal et j'ai ajouté un plan que j'ai légèrement incliné pour obtenir un dossier plus agréable (voir dessin 2).

![](images/idee1et2.png)  

résultat impression 3D de l'idée 1:  
![](images/idee1.jpg)  

résultat impression 3D de l'idée 2:  
![](images/idee2.jpg)  

Pour penser un peu plus dans la direction du designer de l'objet que j'avais choisi, j'ai cherché un moyen de travailler avec des courbes. C'est de là qu'est venue ma troisième idée. Le corps est maintenant transformé d'un plateau droit en un plateau courbe. Cela me semble également être une façon plus agréable de s'asseoir.   
![](images/idee3.png)  

résultat impression 3D de la dernière idée:  
![](images/idee3.jpg)  

Les trois idées:  
![](images/3idees.jpeg)  

<iframe src="https://myhub.autodesk360.com/ue2d88596/shares/public/SH56a43QTfd62c1cd968eab7d2d116882954?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>  

### L'assise ergonomique  
(semaine du 03/12/20)  

Sur recommandation, je me suis plongé dans le thème de l'assise ergonomique. [L'ergonomie](https://fr.wikipedia.org/wiki/Ergonomie) est l'étude scientifique de l'homme en relation avec son environnement. Il peut s'agir d'un produit, d'un espace ou d'un lieu de travail. Sur le dessin ci-dessous, vous voyez les tailles moyennes pour une chaise droite et une chaise droite avec accoudoirs. Comme la chaise que je conçois est destinée aux enfants, ces tailles standard ne sont pas vraiment importantes pour moi. Cependant, je cherche le bon angle pour s'asseoir le plus confortablement possible. L'angle d'un dossier confortable est compris entre 5 et 15 degrés.  

![](images/chair.jpg)  

Pour améliorer le design de la semaine dernière, j'ai continué avec les deux diagrammes suivants. Dans le premier schéma, 6 positions de siège différentes sont proposées. Le premier type est presque entièrement horizontal (3°). Ce position est adapté pour manger ou travailler à un bureau. Le dernier type a un siège très incliné à un angle de 22 degrés. Cette position assure une pression bien répartie sur le siège et le dossier, de sorte que la circulation sanguine est aussi libre que possible. Le type 2 est adapté aux sièges de voiture et les types 3, 4 et 5 sont utilisés pour les chaises de repos.  

![](images/ohara2.jpg)  
[source](https://www.monsiegeamoi.com/blog/en/2020/01/06/the-ideal-angles-for-a-chair/)  

![](images/rittercurve.jpg)  
[source](https://www.core77.com/posts/43422/Reference-Common-Dimensions-Angles-and-Heights-for-Seating-Designers)  

Je suis travaillé sur le type 5. L'inclinaison du siège est 15 degrés et l'angle entre le siège et le dossier est 106,5 degrés. J'ai commencé à dessiner un siège selon le type 5 (voir dessin 1), puis j'en ai fait une ligne fluide (voir dessins 2 et 3).  

![](images/proces.jpg)  

résultat impression 3D:  
![](images/2dec.jpg)


<iframe src="https://myhub.autodesk360.com/ue2d88596/shares/public/SH56a43QTfd62c1cd96885721ea043f6c22c?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>  

#### Comment produire à l'échelle 1:1?  
J'ai déjà commencé à réfléchir à un moyen de fabriquer la chaise à l'échelle 1 sur 1. Faire une surface courbe me semble être la partie la plus difficile. Une possibilité pourrait être de découper plusieurs profils courbes avec la découpeuse laser et de les fixer les uns aux autres avec de la colle à bois. Comme renforts supplémentaires, 3 tiges pourraient être fixées à travers les profils. A voir ce que cela donnerait...  
![](images/planLasercut.jpg)  

### Le siège ergonomique et réglable  
(semaine du 10/12/20)  

Après les conseils de la semaine dernière, j'ai laissé le siège incurvé derrière moi et j'ai commencé à étudier deux nouvelles pistes:  
- Une chaise qui change de fonction lorsque on la tourne, l'empile ou l'assemble.  
- Une chaise dont l'assise peut prendre différents angles au moyen de connections.  

![](images/Harmoire.jpg)  
![](images/chiaseTableArmoire.jpg)  

Je n'ai fait que des esquisses de la première idée (voir l'image ci-dessus) car la deuxième idée d'une chaise avec assise et dossier réglables me semblait plus ambitieuse. Tout ce qui va suivre maintenant portera donc sur cette dernière idéé d'une chaise réglable. Je l'ai fait en trois étapes avec comme résultat 3 prototypes différents à l'échelle 1:5, en bois contreplaqué de 3mm d'épaisseur.  

PROTOTYPE 1:  
Le premier prototype que j'ai réalisé est une chaise qui n'est pas encore réglable, car j'ai d'abord cherché un moyen de fixer les différentes surfaces les unes aux autres. Il était en fait plus un exercice sur les différents connexions. La conception se compose de quatre surfaces: deux surfaces latérales, une surface d'assise et un dossier. Les quatre surfaces peuvent être reliés entre eux en les faisant glisser ensemble à l'aide des fentes prévues.  

![](images/prototype1.jpg)  

PROTOTYPE 2:  
Ensuite, j'ai commencé à chercher un moyen de modifier l'angle du siège et du dossier. Pour cela, je me suis basé sur le diagramme avec les 6 différentes positions assises que j'ai mentionné la semaine dernière. Pour commencer, j'ai décidé de ne changer pour l'instant que la position 1 et la position 5 (voir image). Ce qui signifie que la chaise peut changer d'une position de travail à une position de repos. Pour que cela soit possible, il me faillait un moyen de faire pivoter le siège et le dossier.  

![](images/hoeken.jpg)    

Sur la recommandation de mon frère, j'ai visité le site [Design To Connect](http://designtoconnect.blogspot.com/), un site web intéressant où sont rassemblées différentes connexions dans différents types de matériel. Je suis tombé sur une façon relativement simple de faire pivoter une surface en bois. C'est sur cette référence (voir photo) que j'ai basé la conception du deuxième prototype.  

![](images/systemePivot.jpg)  
Source: [Kerf wall storage bin](https://kerfdesign.com/kerf-wall/)  

Le prototype 2 se compose également d'un siège et d'un dossier. Ces deux surfaces sont reliées l'une l'autre par un assemblage à queue droite. En outre, il y a également deux surfaces latérales dans lesquelles est prévu un trou circulaire qui servira de point de pivot et deux autres trous qui serviront à insérer une barre qui peut être déplacée selon l'angle de la position du siège. Par rapport au prototype 1, le prototype 2 est une chaise à bascule.   

![](images/prototype2.jpg)  

PROTOTYPE 3:  
Pour le dernier prototype, j'ai cherché un moyen plus élégante de déplacer le barre entre les deux trous de positions. J'ai également cherché un moyen de cacher les différents trous sur les surfaces latérales. J'ai été inspiré par un jouet d'un terrain de jeu où des objets peuvent être déplacés en suivant une trace sans être démontés (voir image).  

![](images/jeuLabyrinthe.jpg)  

Pour y parvenir, j'ai travaillé avec 3 couches pour les surfaces des côtés. Une surface sert de finition sur l'extérieur, une deuxième couche veille à ce que la partie mobile reste dans son rail et une troisième couche guide la barre mobile (voir image 3). Ici aussi, l'assise et le dossier sont fixés l'un à l'autre par un assemblage à queue droite (voir image 2). La photo 4 montre les deux couches intérieures. La couche de finition manque encore sur cette photo. Le résultat était plutôt inélégant et lourd.  

![](images/stappen.jpg)  

![](images/prototype3.jpg)  

Les trois prototypes ensembles:  
![](images/driestoel.jpeg)  

### Premier essai à l'échelle 1:1  
(semaine du 17/12/20)  

Comme les deux derniers prototypes de la semaine dernière avec dossier et siège réglables n'étaient pas aussi élégants que le premier prototype, on ma conseillé de continuer à travailler sur le prototype 1. J'ai commencé à adapter la conception et à ajuster les dimensions en fonction de la taille d' un enfant. Pour cela, j'ai utilisé le site web '[DINBelg](http://www.dinbelg.be/2jaartotaal.htm)'. Sur ce site web sont recueillies les mensurations moyennes de la population belge, subdivisées par âge. Je me suis concentré sur les tailles moyennes des enfants de 3, 4 et 5 ans. Les dimensions que j'ai prises en compte sont celles de la hauteur de la surface d'assise de la couronne (n°7), de la hauteur du creux du genou (n°11) et de la profondeur du genou de la fesse (n° 13).  

![](images/schemaKinderen.jpg)  

En tenant compte de la taille d'un enfant, le modèle est réduit. Les dimensions qui sont maintenant utilisées peuvent être lues dans le dessin ci-dessous. La largeur des fentes est égale à l'épaisseur du matériau  dont est faite la chaise. Dans ce cas, un contreplaqué de 12 mm d'épaisseur sera utilisé. L'inclinaison du siège et du dossier est toujours basée sur le diagramme avec les 5 différentes positions d'assises. J'ai choisi d'appliquer le type 4 à ce prototype car cette valeurs se situe au milieu de la position de travail et de la position de repos. Les deux poignées sont situées dans le siège et le dossier. Cela a été fait pour que ces deux surfaces puissent facilement être glissées l'une dans l'autre, mais aussi pour qu'il soit possible de soulever et de déplacer la chaise facilement.

![](images/afmetingenStoel.jpg)  

Après avoir dessiné le design, il était temps de réfléchir à la façon dont il était possible de fabriquer la chaise. Comme le contreplaqué de 12 mm est déjà assez épais pour être découpé avec le lasercutter du Fablab, j'ai essayé de travailler avec un lasercutter plus puissant de quelqu'un que je connais. j'ai essayé avec différents réglages (plus de puissance, plus rapide, en plusieurs étapes,..) de faire passer le laser à travers le bois mais une fois que j'ai réussi à couper le bois, le résultat était très brûlé et donc plus utilisable. Un **petit conseil** pour éviter les taches brûlées sur le dessus du bois avec le laser: mettez du ruban de masquage sur la surface de votre matériel!
![](images/testLasercutter.jpg)  

La deuxième option que j'avais pour couper le bois était de travailler avec le CNC, Shaper Origin. Comme je n'étais pas encore très familier avec cette machine, j'ai commencé à couper avec quelques test. De cette façon, je pouvais tester l'effet des différentes mèches, comparer le résultat de la découpe en différentes passes, voir si je devais calculer des marges dans mon dessin, etc.  

![](images/testShaper.jpg)  

En fin de compte, j'ai obtenu le meilleur résultat en coupant les fentes avec une mèche de 3 mm car il était important qu'il y ait le moins d'arrondis possible dans les fentes. C'est un effet standard du Shaper lorsque on coupe le long de l'intérieur de la ligne. J'ai fait le fraisage avec la mèche de 3 mm en quatre fois. Pour savoir combien de fois vous devez couper, vous divisez l'épaisseur de votre bois par le diamètre de la mèche. Pour les contours, j'ai utilisé la mèche de 6 mm. C'était plus rapide car je n'ai eu à recouper que deux fois. Une explication détaillée sur la façon d'utiliser le Shaper se trouve dans le module 5. Et voilà le résultat!  

Les différentes parties, non encore assemblées:   
![](images/prototypeSchaal1c.jpeg)  

Après l'assemblage:  
![](images/prototypeSchaal1a.jpeg)  

Au total, la chaise pèse actuellement 3,5 kg. En général, toutes les pièces s'emboîtent bien, sauf à la hauteur de la fente du dossier où le siège se glisse. Comme ces deux surfaces ne glissent pas perpendiculairement l'une à l'autre, j'aurais dû agrandir la fente (voir photo ci-dessous). Je ne suis pas encore sûr que l'emplacement des poignées soit le meilleur choix pour le moment. Si vous souleviez la chaise d'une main à travers la poignée située au dossier, vous auriez l'impression que cette surface pourrait glisser.  
![](images/prototypeSchaal1b.jpg)   


### La dernière ligne droite
(semaine du 11/01/21)  

La semaine dernière, j'ai cherché une fonction supplémentaire pour le président. Quel est l'effet de la rotation de la chaise, de déplacement d'une surface ou d'ajouter une surface supplémentaire? Pour un premier test, j'ai ajouté une surface supplémentaire qui peut servir de table. En faisant une fente supplémentaire sur les deux côtés, on peut faire glisser une surface supplémentaire dans la chaise. La connexion est conçue de telle manière qu'un enfant assis sur la chaise ne peut pas pousser la surface de la table.  

![](images/tafelblad.jpg)  

Pour une deuxième idée, une fente supplémentaire a été ajoutée dans chaque surface latérale, permettant de placer le dossier à l'horizontale lorsque la chaise est tournée de 90 degrés. De cette façon, une petite table est créée. Inconvénient de ce modèle, la même chaise ne pourrait pas se glisser sous la table car les deux ont la même largeur.   

Une troisième idée est que la chaise peut aussi devenir un marchepied. Pour ce faire, deux emplacements supplémentaires sont prévus de chaque côté. Cela permet de rentrer horizontalement la surface du siège et le dossier. Si on tourne maintenant la chaise de 180 degrés, on obtient un marchepied. Après la réalisation de ce prototype, il est apparu que le marchepied n'est pas si solide puisqu'il n'y a qu'une connexion dans l'axe horizontal.

![](images/marchepied.jpeg)  

La chaise est testée par trois âges différents. La fille en jaune a 2 ans, le garçon avec le t-shirt vert a 4 ans et le garçon avec le pull bleu a 8 ans. Il est clair que les dimensions utilisées sont correctes et que la chaise est la plus adaptée aux enfants entre 3 et 5 ans.  

![](images/testkindjes.jpg)

<iframe src="https://myhub.autodesk360.com/ue2d88596/shares/public/SH56a43QTfd62c1cd9681bbee0f5d03cd9ea?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>  

Pour la version finale de la chaise, j'ai laissé les poignées hors de la surface de l'assise et du dossier car elles glisseraient hors de la fente si vous preniez la chaise par l'une des poignées. Dans le prototype final ci-dessus, vous pouvez voir que les poignées sont placées sur les côtés, mais je n'étais pas fan de cela car c'était trop encombré puisqu'il y a déjà 4 fentes sur les côtés.  
J'ai également arrondi les coins du modèle final pour créer un siège plus sûr et plus adapté aux enfants.  
![](images/shapervormen.jpg)  

![](images/workinprogress2.jpg)  

![](images/resultat.jpg)  

#### Expérimentation avec d'autres matériaux  
Comme l'objet sur lequel je me suis basé (H-Horse) est en polycarbonate, il m'a semblé approprié de fabriquer la chaise également dans un matériau similaire. Après quelques recherches sur internet, le plexiglas semblait être un bon choix, mais il s'est avéré très cher si vous souhaitez une certaine épaisseur. Heureusement, j'ai pu récupérer une plaque de plexiglas qui était au boulot de mon père. Comme cette plaque fait 6 mm d'épaisseur, j'ai (la chaise en bois a une épaisseur de 12 mm). Ce prototype a été réalisé avec un [autre lasercutter](https://www.instagram.com/betty.the.laser.cutter/?hl=nl) que celui de la Fablab puisque j'ai eu la plaque de plexiglas à la dernière minute. Avec le lasercutter, vous obtenez un très beau résultat lorsque vous coupez du plexiglas. Pas de bords bruns ou fondus! Après avoir vu le résultat, j'ai pensé qu'il serait possible de rendre la chaise en grandeur réelle. Le plexiglas semble très solide. Malheureusement, je n'ai pas pris assez de plexiglas avec moi pour le refaire.  

![](images/plexi1.jpg)

![](images/plexi2.jpg)  

### Comment construire la Chaise H?  

![](images/commentConstruire.jpg)  
