# Qui suis-je
Salut! Je m'appelle **Maud**. Je suis étudiante en architecture en deuxième master. Pendant ce trimestre, je suis des cours à L’ULB en tant qu’étudiante Erasmus.  Pourtant, je suis d’origine Belge,  mais je fais un 'Erasmus Belgica'. Ça veut dire que je fais un Erasmus dans mon propre pays, la Belgique, mais dans une université Francophone.  
Il y a quatre ans, j'ai commencé mes études d'architecture à la KUL au [campus de Sint-Lucas Bruxelles](https://arch.kuleuven.be/contact/campus-sint-lucas-brussel).
L'année dernière je faisais un échange Erasmus en Italie dans la ville _Genova_, située dans le nord de l'Italie. A cause de corona, l'échange a durée moins longtemps que prévue. Pourtant, c'était une expérience très intéressante et enrichissante.  

![](images/TeteMaud01.jpg)

## Un peu plus d'infos
J'ai grandi à Zaventem. J'y ai fait mes études primaire et secondaire.
Jusqu'à l'année dernière, j'étais chez les scouts de Zaventem et j'ai suivi des cours de guitare à l'école de musique pendant dix ans. Mes autres loisirs ou activités préférées sont: dessiner, bricoler, coudre, fabriquer des bijoux, cuisiner, manger, faire des maquettes,... En bref, j'aime travailler avec mes mains et être créatif. Je suis donc très intéressée à apprendre de nouvelles choses dans ce contexte.  
Comme j'ai plus de temps depuis cette année, parce que je ne vais plus aux scouts tous les dimanches, j'ai commencé un nouveau projet. Avec mon copain, nous construisons un four à pizza dans le jardin.  

![](images/maquettes.png)  
_Quelques maquettes, réalisées par moi-même pendant les années passées._

## Option architecture et design

### Exercice du fabzéro
Pendant le premier cours d'option design nous avons visité le Design Museum Brussels. On nous a demandé de choisir un objet de l'exposition. Notre projet final sera une ré-interprétation de cet objet. Pendant la première partie du trimestre, nous apprendrons à travailler avec les machines du Fablab. Pendant la prochaine phase, nous aurons l'occasion de travailler avec nos nouvelles connaissances sur la ré-interprétation de l'objet choisi.

### Choix de l'objet au musée
J'ai choisi le cheval à bascule minimaliste conçu par Oki Sato. Contrairement au cheval à bascule classique en bois, ce cheval est composé de trois parties courbes transparentes en polycarbonate.  
Cet objet a retenu mon attention car, contrairement à la plupart des objets du musée, ce n’est pas un meuble typique, c’est un jouet. L'environnement du musée donne au jouet une atmosphère minimaliste et statique. On n'oserait presque pas jouer avec. Mais retirez l'objet du contexte 'musée', mettez-le dans une pièce avec des enfants et l'objet aura un air complètement différent. Je trouve intéressant que, malgré l'image classique d'un cheval à bascule en bois, le choix du polycarbonate transparent n'enlève rien à la reconnaissance d'un cheval à bascule.
La transformation d’un objet structurel en un jouet ludique sans faire disparaître la simplicité de l’objet original est à mon avis une idée forte. Egalement l'apparence de robustesse est maintenue par la composition bien pensée des trois parties qui composent le cheval à bascule.   
![](images/HorseMuseum.jpg)
