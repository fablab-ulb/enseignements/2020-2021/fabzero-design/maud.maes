# 4. Découpe assistée par ordinateur

Afin de nous faire découvrir les deux machines à découpe laser du Fablab, on reçoit d'abord une brève explication sur les deux machines. Puis on nous demande de concevoir une lampe que nous pouvons ensuite faire couper avec le lasercutter. Le matériau que nous obtenons pour cela est le polypropylène. Il s'agit d'une feuille mate et transparente d'environ 1mm d'épaisseur.

## Les machines
Le Fablab dispose de deux découpes laser: le Lasersaur et le Muse FullSpectrum. La plus grande différence entre les deux est la taille de la surface de coupe.
###### Spécifications du Lasersaur:
surface de découpe: 122 x 61 cm  
Hauteur maximum: 12cm  
Vitesse maximum: 6000 mm/min  
Puissance du Laser: 100W  
Type de Laser: Tube CO2 (infrarouge)  
L'interface utilisateur: Drive BoardApp  

Selon le type de matériau que tu souhaites découper, tu peux ajuster la vitesse du laser. Cependant, il n'est pas recommandé de couper à la vitesse maximale, car cela peut donner un résultat  imprécis. (Tu peux trouver plus d'informations sur la vitesse spécifique au type de matériau sous la titre 'Les Matériau'.) L'interface utilisateur nécessaire pour utiliser la machine est le **Drive BoardApp**. Cette machine peut uniquement traiter des fichiers vectoriel. Il est donc important que votre dossier est un fichiers SVG ou DXF. Un fichier SVG est préférable car il est généralement plus facile à utiliser. Assures-toi de travailler avec des couleurs RGB! Pour importer une image matricielle, il faut l’intégrer dans un fichier SVG.  

###### Spécifications du Muse Full FullSpectrum:
surface de découpe: 50 x 30 cm  
Hauteur maximum: 6cm  
Puissance du Laser: 40W  
Type de Laser: Tube CO2 (infrarouge) + Pointeur rouge   

La taille de surface de coupe de cette machine est plus petite que la taille du Lasersaur. En raison de la puissance plus faible de cette machine, elle est plus souvent utilisée pour la gravure. l'interface utilisée est **Retina Engrave** et ici aussi, il est recommandé de travailler avec des fichiers SVG. Lorsque tu importes un fichier, le fichier sera importé en vectoriel mais aussi en matriciel, il faudra donc en supprimer un des deux. La machine peut également traiter des images matricielles comme BMP, JPEG, PNG, TIFF.  

## Gravure, découpe ou marquage  
* **La gravure**: en gravure (d'une forme/image) la machine va fonctionner un peu de la façon qu'une imprimante jet d'encre en balayant toute la surface à graver. Ce processus prend de temps.  

* **La découpe**: lors de découpe, le laser se déplace avec suffisamment d puissance le long des lignes indiquées pour la découpe jusqu'à ce qu'il coupe à travers le matériau.  

* **Le marquage** est en fait la même opération que la découpe, mais des réglages de faible puissance. Par conséquent, il ne coupe pas le matériau.  

Pour faire la différence entre le marquage et la découpe, tu dois donner aux lignes à marquer une couleur différente de celle des lignes à découper. Ensuite, tu peux régler la pression (%) et la vitesse (F) de chaque couleur dans l'interface du lasercutter.  
![](../images/gravureDecoupe.jpg)  
Tu peux trouver plus d'informations [ici](http://carrefour-numerique.cite-sciences.fr/fablab/wiki/doku.php?id=machines:decoupe_laser:0_utilisation:preparation_fichier_decoupe).

## Les matériaux
Tu dois faire attention au matériau que tu utilises car tous les matériaux ne sont pas sans danger pour le laser.  

* Les matériaux recommandés:  
Bois contreplaqué (plywood/multiplex), Acrylique (PMMA/Plexiglass), Papier, carton, Textiles.  

* Les matériaux déconseillés:  
MDF (fumée épaisse, et très nocive à long terme), ABS et PS (fond facilement, fumée nocive), PE, PET et PP (fond facilement), Composites à base de fibres (poussières très nocives), Métaux (impossible à découper)  

* Les matériaux interdits:  
PVC (fumée acide, et très nocive), Cuivre (réfléchit totalement le LASER), Téflon (PTFE) (fumée acide, et très nocive), Vinyl et simili-cuir (peut contenir de la chlorine), Résine phénolique et époxy (fumée très nocive)  

Selon le type de matériau que tu utilises, tu dois ajuster la puissance et la vitesse du Lasersaur. [Ce lien](https://wiki.chantierlibre.org/machines:lasersaur:reglages_lasersaur) te permets de trouver une collection de différents types de matériaux et leurs paramètres pour le Lasersaur.

## Préparer un fichier pour la découpe laser  
Un dessin matriciel est encodé en utilisant des pixels. Ceci veut dire qu'une fois sauvegarder il a une résolution maximale et donc quand on zoom trop profondément on verra des artefacts comme dans photo n°1. Ceci permets de compressé des images pour qu'elle prennent moins de place. Tandis que un dessin vectoriel est encodé en utilisant des fonctions mathématique pour définir des ligne. Comme ces fonctions sont continu, il a une résolution infinie. Ceci garanti des dessins plus correctes à chaque échelle (voir photo n°2). Pour la découpe laser, on a besoin des dessins **vectoriel**. Les machines ne savent pas identifier les contours des dessins à partir d'un fichier matriciel.  
![](../images/Vector_vs_raster.png)  

Il existe de nombreux programmes différents avec lesquels tu peux dessiner ton design, pensez à Fusion 360, Autocad, Vectorworks, Illustrator,... Le plus important est que tu peux exporter ton fichier en format SVG (de préférence) ou DXF. Si tu n'as pas dessiné avec Illustrator, il est recommandé d'ouvrir ton fichier dans **Illustrator** or **Inkscape** (à télécharger gratuitement [ici](https://inkscape.org/release/inkscape-1.0.1/)). Avec ces programmes, tu peux attribuer des couleurs à tes lignes. Assures-toi de travailler dans l'espace couleurs **RGB** et non CMYK! (Voir photo n°1) Tu peux facilement dégrouper et dédoubler les lignes si c'est nécessaire. Je fais cela dans Illustrator de la manière suivante:  
1. Si tu as plusieurs couleurs: sélectionnez une ligne avec une couleur spécifique  
2. Utilisez l'outil  Select > Same > Stroke Color  
3. Ensuite, choisissez le menu Object > Live Paint > Make (voir photo n°2)  
4. Choisissez le même menu Object > Live Paint > Expand  (voir photo n°3)  
5. Enfin, utilises le menu Object > Ungroup pour dégrouper toutes les lignes. (voir photo n°4)  
6. Répétez les étapes 1 à 5 si tu as plusieurs couleurs.  

![](../images/illustrator.jpg)  

J'ai dessiné mon design avec Fusion 360 et je l'ai exporté sous forme de fichier DXF. Ce fichier je pourrais ensuite importer dans Illustrator. Ici, j'ai dédoublé et dégroupé mes lignes. Je leur ai donné les couleurs qu'ils devaient avoir. Pour finir j'ai exporté ce fichier vers un fichier SVG.  

Quand je voulais ouvrir mon fichier avec le Drive boardApp pour l'envoyer au laser, j'ai reçu un message d'erreur. Je ne sais pas pourquoi je ne pouvais pas pu ouvrir mon dossier, mais j'ai finalement trouvé une alternative. Après avoir exporté mon fichier de Fusion 360 dans un fichier DXF (voir photo), j'ai importé mon fichier dans Illustrator. Par Illustrator, j'ai converti mon fichier en fichier SVG et j'ai finalement pu ouvrir ce fichier avec Inkscape. J'ai ensuite en Inkscape, donné aux lignes la bonne couleur. Je ne sais pas encore s'il existe un moyen rapide de dédoubler les lignes dans Inkscape, alors je les ai vérifiées une par une. Ce fichier SVG que j'ai finalement obtenu pouvait être ouvert avec le drive boardApp.  
![](../images/fusion360toDXF.jpg)  

## La Lampe

### Modèl de pliage 2D
Le modèl de la lampe est un modèle de pliage, consistant en une base octogonal avec des éléments trapézoïdaux sur les deux côtés. Les surfaces trapézoïdales sont pliés ensemble et connecté par un joint entre une flèche et fente. Avant de dessiner le modèle en Fusion 360, j'ai d'abord fait une petite version d'essai en papier.  

![](../images/modelePapier1.jpeg)  

Une fois que le modèle de pliage est dessiné en Fusion 360, il est exporté dans un fichier DXF.

![](../images/SchermafbeeldingModelePlisse.png)

![](../images/lampModelePlisse.jpg)  

Pour m'assurer que les connexions sont bien alignées et assez grandes, j'ai fait découper le modèle 2D par mon Cameo. Au premier modèle de test sur papier, les fentes pour la flèche étaient trop petites pour y entrer. J'ai ensuite ajusté cela dans mon modèle en 2D sur Fusion 360.  

![](../images/modelePapier2.jpg)      

### Modèl 3D
Pour dessiner le modèle 3D, j'ai commencé à extruder le modèle de pliage 2D avec 1mm (l'épaisseur de la feuille de polypropylène). Ensuite, j'ai fait tourner les 8 surfaces à 45° pour obtenir une forme octogonal. Puis, j'ai pivoté chaque surface trapézoïdale avec 70° vers l'intérieur.   

![](../images/SchermafbeeldingLampe1.png)  

![](../images/SchermafbeeldingLampe2.png)  

![](../images/SchermafbeeldingLampe3.png)  

<iframe src="https://myhub.autodesk360.com/ue2d88596/shares/public/SH56a43QTfd62c1cd9682e26bdc29396b3e8?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>  


### Découpé au laser

Suivez [ce manuel](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/raw/master/files/Manuel_Lasersaur.pdf?inline=false) pour préparer le découpeur le laser. Dans ce qui suit, j'expliquerai mon expérience et les problèmes que j'ai rencontrés lors de la préparation au laser.   

Une fois que j'ai pu ouvrir mon fichier dans le drive boardApp (voir mes problèmes précédentes sous la partie 'Préparer un fichier pour la découpe laser') j'ai découvert un nouveau problème. Mon dessin était beaucoup trop grand car les unités étaient passées du millimètre au mètre. Pour résoudre ce problème, je suis retourné à Inkscape. J'ai cliqué sur file > document properties et j'ai changé l'unité de pixels en millimètres (voir photo).  

![](../images/eenheidInkscape.jpg)  

Une fois que mon fichier à été correctement ouvert, je pouvais lier mes différents couleurs de lignes aux bons paramètres. Tu peux le faire en cliquant sur **+** à droite chez 'Pass 1' et en séléctionnant la bonne couleur. Pour moi, le rouge était la couleur des lignes qui n'avaient pas beson d'être coupées. Je les ai donnés comme vitesse (F) 1500 mm/min et comme puissance (%) 20. D'autres personnes qui avaient travaillé avec ce matériau avant moi on dit que ces valeurs étaient bonnes pour la gravure sur polypropylène.
Au 'Pass 2', je choisis la couleur noire pour les lignes qui doivent être coupées. Ici, je choisis comme vitesse 1500 mm/min et comme puissance 46% (voir la photo).  
Il est recommandé de graver d'abord et de découper ensuite. Si cet ordre est inversé, il est possible que des pièces déjà coupées se déplacent pendant la gravure.  

![](../images/IMG_20201104_123907__01.jpg)  

Pour voir si ton dessin n'est pas plus grand que le matériel, tu cliques d'abord sur le bouton avec la petite maison en bas (voir n°1 sur la photo), puis cliques sur les deux flèches à coté de 'Run'(voir n°2 sur la photo). Le laser va maintenant se détacher des bords de ton dessin sans allumer le laser. Si cela te convient, tu peux maintenant cliquer sur 'Run' (voir n°3 sur la photo) pour lancer le laser. Pour cela, le 'Status' (voir n° 4 sur la photo) doit s'allumer en vert. Sinon, le lasercutter ne fonctionnera pas.  

![](../images/DriveboardApp.jpg)    

Respectes toujours les **précautions** suivantes avant de commencer:  
1. Connaître avec certitude quel matériau est découpé.  
2. Toujours ouvrir la vanne d’air comprimé!  
3. Toujours allumer l’extracteur de fumée!  
4. Savoir où est le bouton d’arrêt d’urgence.  
5. Savoir où trouver un extincteur au CO2.  

![](../images/tests.jpeg)  
Sur cette photo, tu vois deux test que j'ai faits pour voir si les ouvertures étaient assez grandes pour les flèches.   

![](../images/plat.jpeg)  
Comme j'avais préalablement testé avec une petite partie de la lampe pour voir si les connexions s'emboîtaient bien, le montage de la lampe a été très facile.

### Le résultat  

![](../images/geplooid.jpeg)  

![](../images/donker.jpg)  
