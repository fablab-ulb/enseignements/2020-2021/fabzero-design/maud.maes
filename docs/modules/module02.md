# 2. Conception assistée par ordinateur  

Pendant le cours de 8 octobre, nous avons reçu une explication de base sur le programme de dessin Fusion 360. Jeudi 15 octobre, nous avons pu pour la première fois concevoir nous-même quelque chose en Fusion 360 et le faire imprimer par l'imprimante 3D. Pour en savoir plus sur l'impression 3D, consultes le module 3. Dans module 2, je expliquerai plus en détail les bases que nous avons acquises sur Fusion 360 et je monterai ensuite comment j'ai réalisé mon premier dessin sur Fusion 360.  

## Introduction Fusion 360
Je résume ci-dessous les outils les plus couramment utilisés par moi de la Fusion 360.  

1. **Create sketch**: Pour faire un nouveau dessin en 2D fais d'abord une nouvelle esquisse. Cette nouvelle esquisse est une nouvelle couche dans laquelle tu vas dessiner.  Cela peut être fait avec le bouton 'create sketch' (voir n° 1).  

2. **Create**: Les éléments 2D de base, tels qu'une ligne (L), un rectangle , un cercle, un arc,... se trouvent sous le bouton 'create' (voir n° 2).  

3. **Modify**: Sous 'modify' tu peux modifier l'esquisse dessinée.(voir n° 3).  
  . Fillet: arrondir des angles  
  . Trim (T): couper une courbe d'esquisse à la courbe d'intersection ou à la géométrie boudaire la plus proche  
  . Sketch scale: augmenter/réduire l'échelle de l'esquisse  
  . Move/copy (M): déplace le 'face', 'body', 'sketch' ou le construction d'une distance ou d'un angle déterminé.  

4. **Constraints**: ou contraintes te permets de relier une entité d'esquisse à une autre entité d'esquisse. Les contraintes d'esquisses utilisent des expressions géométriques comme horizontal/vertical, coïncident, tangent, égal, parallèle, perpendiculaire,... (voir n° 4).  

5. **Inspect**: Avec la fonction 'inspect'(I) tu peux mesurer la distance, l'angle, la surface ou la position des objects sélectionnés (voir n° 5.).  

6. **Finish sketch**: Une fois que tu as terminé ton dessin en 2D, n'oublier pas de terminer la esquisse en cliquant sur le bouton 'finish sketch' (voir n° 6).  

7. **Extrude**: Tu peux extruder (E) l'objet dessiné en 2D une fois que vous avez quitté l'environnement de l'esquisse. Pour faire ceci, vas au 'create' et cliquez sur 'extrude' (voir n° 7). Maintenant une écran apparaîtra pour te demander de sélectionner l'objet que tu souhaites extruder (voir n° 7b). Il y a aussi d'autres fonctions pour spécifier la extrusion:  
  . Le point de départ  
  . La direction de ta extrusion: à sens unique, à double sens ou symétrique  
  .  Le point de fin  
  .  La distance de la extrusion  
  .  L'angle   
  .  L'opération: joindre, couper, croiser, nouveau body, nouvelle composante  

8. **Pattern**: Avec 'pattern', tu peux duplique des extrusions, faces, bodies,... selon un patron rectangulaire, un patron circulaire ou selon un trajet (voir n° 8).  

9. **Revolve**: Au lieu de créer un objet en 3D par extrusion, tu peux également utiliser l'outil 'revolve' (voir n° 9). Avec cet outil, tu peux faire tourner un croquis 2D  autour d'un axe sélectionné.  

10. **Sweep**: Cette fonction permet de balayer un profil 2D ou une face plane le long d'un chemin   choisi. Pour ce faire, sélectes une série de profils ou de faces pour définir une forme. (voir n°10)  

11. **Loft**: Cette fonction crée une forme de transition entre deux ou plusieurs profils 2D ou faces planes.  

12. **Render**: Tu peux changer l'apparence de votre objet en allant sur 'render' et après cliquer sur 'appearance' Ici tu peux sélectionner un type de matériau et le faire glisser vers l'objet que tu veux modifier (voir n° 12).  

![](../images/baseFusion.jpg)  

## Modeler mon objet
Sur [le site de Kartell](https://www.kartell.com/CH/nendo/h-horse/03270), le bureau de design qui vend le cheval à bascule, j'ai trouvé des fichiers 2D et 3D de mon objet qui pouvaient être téléchargés gratuitement. Pour savoir comment télécharger le fichier 3D dans Fusion 360, j'ai visité [ce site](https://knowledge.autodesk.com/support/fusion-360/learn-explore/caas/sfdcarticles/sfdcarticles/How-to-import-DWG-files-into-Fusion-360.html#:~:text=Both%202D%20and%203D%20DWG,and%20edited%20in%20Fusion%20360.&text=Fusion%20360%20can%20only%20import,geometry%20or%203D%20solid%20bodies.).

Finalement, j'ai décidé de dessiner moi-même mon modèle 3D. Je ne savais pas comment modifier le fichier 3D de Kartel et il me semblait plus intéressant de m'entraîner à utiliser Fusion 360.  

Pour dessiner mon modèle, j'ai d'abord dessiné trois formes en 2D que j'ai ensuite extrudées. Ces trois formes (voir photo) sont également les trois formes de base que le designer a rassemblées pour réaliser le cheval à bascule: la courbure du cou (n° 1), la partie centrale plate (n° 2) et la forme rectangulaire plissée du bas (n° 3).  

![](../images/3parties.jpg)  

Je n'étais pas satisfait du premier modèle que j'ai dessiné. La forme du cou était trop différente de celle de Kartell. Mon dessin était également composé de 2 corps au lieu d'un seul corps. C'est pourquoi j'ai décidé de redessiner mon modèle, mais cette fois-ci façon plus réfléchie.    
Dessin 1:  
![](../images/SchermafbeeldingModel1.jpg)    
Dessin 2:  
![](../images/SchermafbeeldingModel2.jpg)  
Plus tard, quelqu'un m'a dit que je pouvais insérer des images dans Fusion 360. Ceci aurait été plus facile pour copier mon object. Le modèle qui a finalement été imprimé n'a past été copié à partir de la photo parce que je ne savais pas encore que je pouvais le faire.  
![](../images/SchermafbeeldingCanvasEtObjet.jpg)  

Pour exporter mon fichier de Fusion 360 vers un fichier STL, j'ai suivi [ce tutoriel](https://knowledge.autodesk.com/support/fusion-360/learn-explore/caas/sfdcarticles/sfdcarticles/How-to-export-an-STL-file-from-Fusion-360.html). C'est nécessaire d'avoir un fichier en format STL pour ouvrir le document avec le programme de la imprimante 3D.

## Modèle 3D  

<iframe src="https://myhub.autodesk360.com/ue2d88596/shares/public/SH56a43QTfd62c1cd968a4bea5085ca78d08?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>  
