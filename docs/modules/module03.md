# 3. Impression 3D  
Dans ce module, je expliquerai les choses les plus importantes que on dois savoir sur les imprimantes 3D disponible dans le Fablab. Je parlerai également des différentes étapes que j'ai suivies pour imprimer mon modèle conçu dans Fusion 360 (voir module 2) avec l'imprimantes 3D.  

## L'imprimante 3D  
Le modèle dont dispose le Fablab es le **Prusa i3 Mk3**. Il se déplace sur les axes x, y et z. Le volume maximal que tu peux atteindre avec ces imprimantes est de 25x21x21 cm et ce à une vitesse maximale de 200 mm/s (ce qui n'est pas recommandé pour éviter que l'imprimante ne fasse des erreurs). Les imprimantes Prusa sont basées sur un système open-source. Cela signifie que tous les codes sources, les pièces imprimées, les plans,... sont accessible librement à tous via [GitHub](https://github.com/prusa3d). Une grande partie des pièces sont imprimées par d'autres imprimantes 3D qui sont située dans un grand hall avec plus de 500 imprimantes, appelé 'the Farm'.  

![](../images/prusa.jpg)  

Le matériau utilisé pour l'impression est appelé filament. Il existe différents types de filaments. Tant le matériau (PLA, bois, ABS, Pet,...) que l'épaisseur ou la couleur de ton filament peuvent être choisis en fonction de votre objet.

## PrusaSlicer
Afin d'imprimer ton modèle 3D, tu dois d'abord le convertir en G-code. En bref, le G-code est un langage utilisé pour dire à une machine comment faire quelque chose. Pour cela, on utilise le programme **PrusaSlicer**. Tu peux télécharger le programme via [ce lien](https://www.prusa3d.com/drivers/). Pour ouvrir un fichier dans PrusaSlicer, que tu as crée avec Fusion 360, tu dois l'exporter de Fusion 360 comme fichier STL.  

* Sur l'interface de PrusaSlicer, à gauche (voir n°1), tu trouves les outils qui permettent de modifier l'emplacement de l'objet sur le plateau.  

* Pour importer ton objet, il suffit de glisser et de déposer ton fichier dans le programme. Ton objet apparaîtra alors sur le plateau (voir n°2).  

* Il est recommandé de sélectionner le mode 'Expert' (voir n°3). Ceci te permet d'avoir accès à plus de paramètres.  

* Les numéros 4, 5 et 6 (voir photo) te permet respectivement de régler les paramètres d'impression, les paramètres du filament et les paramètres de l'imprimante.  

* Assures-toi que tu choisis le bon filament à la section 'filament' (voir n°7). Dans notre cas, il s'agit du PLA.

![](../images/prucaSlicer.jpg)

## La préparation
Pour faire une bonne impression, on va ajuster certains paramètres. Si on modifie des paramètres, cette valeur ne sera plus en noir mais en orange.  

Réglages d'impression  
**Couches et périmètres**: La hauteur des couches et la hauteur de la première couche sont des valeurs invariables correspondant aux dimensions de la buse. Dans notre cas 0,2 mm. Pour avoir une certaine solidité, le nombre de couches verticales et horizontales doit être au moins 3.   
![](../images/couchesPerimetres.png)    

**Remplissage**: La densité de remplissage varie selon l'objet. Attention, cette valeur ne doit jamais dépasser 35%. Le motif de remplissage dépend également du objet en fonction de la résistance que l'objet a besoin.  
![](../images/remplissage.png)    

**Jupe et bordure**: La jupe définit le périmètre d'impression sur le plateau. Cela fournit une surface stable pour l'objet. Supposons que tu aies un objet de grande taille, il est bon de faire un contour supplémentaire pour empêcher l'objet de tomber.  
La bordure permet de 'solidifier' la base de l'objet. C'est un atout supplémentaire sur le plateau autour du objet, qui est ajouté pour encore plus de solidité.  
![](../images/jupeBordure.png)    

**Support**: Les supports permettent de pallier des ponts. Si 'générer des supports' est cochée, les supports seront générés automatiquement. Parfois, certains supports ne sont pas indispensables, cocher 'support sur le plateau uniquement' permet donc de faire 'une sélection' des supports nécessaires.  
![](../images/support.png)    

Réglages du filament  
Si tu as indiqué le filament correct sur l'écran principal, ces valeurs seront remplies automatiquement. Il est préférable de vérifier si tes paramètres correspondent.
![](../images/filament.png)  

## Imprimer mon objet  
Avant d'envoyer mon modèle à l'imprimante 3D, j'ai essayé quelques moyens de le positionner. Finalement, j'ai positionné mon modèle verticalement au lieu de horizontalement, proposé par les Fabmanager's.  
![](../images/SchermafbeeldingVertical.jpg)  

Pour voir à quoi ressemble le support, tu peux cliquer dans le coin gauche sur le symbole du cube avec les différentes couches.  
Dans le coin inférieur droit, tu peux voir combien de temps il faudra à l'imprimante pour créer l'objet. Dans mon cas, cela a pris près de deux heures pour la première impression.  
Si tous les paramètres sont correctement réglés, tu peux exporter ton fichier vers un fichier de G-code. Pour faire ceci, tu cliques sur le bouton dans le coin inférieur droit.  

![](../images/gcode.png)  

Tu peux maintenant mettre le fichier G-code sur une carte SD que tu peux mettre dans l'imprimante 3D. Avant lancer l'impression, nettoyer le plateau avec de l'acetone. Puis vérifies sur l'écran les températures et la vitesse. Il est recommandé de ne pas laisser l'imprimante imprimer plus vite que 100% afin d'éviter les inexactitudes et les erreurs. Lorsque ton fichier se lance, restes à côté de la machine pendant les 3 premiers couches de l'impression.

![](../images/print1.jpg)  

## Le résultat
Résultat impression 1:  
![](../images/3Dprint1.jpeg)  

Le résultat est bon mais au niveau du cou il y a quelques défauts à cause du positionnement et du supports. Ceci était partiellement à cause de mon design assez fin du cou. Après avoir montré l résultat à mon frère et mon copain, ils m'ont conseillé de le refaire en l'impriment horizontalement cette fois. De cette manière, l'impression prend également deux fois moins de temps que la première fois. Vue qu'on a une imprimante 3D à la maison, je l'ai alors refait et le résultat était meilleur.  
![](../images/SchermafbeeldingHorizontal.jpg)  

Résultat impression 2:  
![](../images/3Dprint2.jpeg)  

Comparaison des deux impressions:  
![](../images/3Dprint1and2.jpeg)
