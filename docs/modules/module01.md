# 1. Etat de l'art et documentation  
Pendant la deuxième semaine, nous nous sommes rencontrés pour la première fois dans le Fablab. La cours a été divisée en deux parties. Le matin, on nous a enseigné la base du programme **Fusion 360** et pendant l'après-midi, on nous a donné une explications sur **Gitlab**, la plateforme que nous utiliserons pour documenter et partager notre travail. Plus d'informations sur l'utilisation de Fusion 360 peuvent être trouvées dans le module 02. Dans ce module, je vais approfondir l'utilisation de Gitlab.

## Configuration de Gitlab

Le format final de notre documentation est un site internet web. Pour arriver à ceci on utilise la langage Markdown. Markdown est facile à écrire, à lire et facilement à exporter en HTML ou dans d'autres formats. [Ce tutoriel](https://www.markdowntutorial.com/) explique les éléments clés de la langage en 10 minutes. Avec Markdown, je peux écrire dans un éditeur de texte. On utilise Atom (voir photo 1). J'ai  téléchargé cet éditeur de texte [ici](https://atom.io/). Avant de pouvoir mettre un texte, écrit dans Atom, sur ma page web, je dois d'abord faire quelques étapes. Ces étapes ne peuvent être réalisées qu'une seule fois. Après effectué ces étapes, je peux toujours éditer et modifier ma page web de manière simple.  
![](../images/atom.jpg)  
(photo 1)  

#### Personnaliser le fichier mkdocs.yml avec mes informations:  
J'ai rempli mon nom aux endroits indiqués (voir photo 2). Ces endroits s'appelle 'namespaces'. Sous 'theme' tu peux modifier le layout de sa page web. J'ai choisi le thème 'lux'. Tu trouve [ici](https://mkdocs.github.io/mkdocs-bootswatch/#lux) un aperçu clair des différents thèmes.  
![](../images/etap1.jpg)  
(photo 2)  

#### Installer 'Git' sur mon ordinateur:  
Git est le protocole le plus utilisé de version control au monde. Ceci nous permet de facilement partager notre code source. Avant de télécharger Git, nous devons d'abord télécharger Homebrew car Homebrew est le packet manager pour Mac.  
  1. Pour télécharger Homebrew écris la ligne de code suivante dans le terminal:  ` $ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)" `  
  2. Tu peux ensuite installer Git en écrivant la ligne de code suivante dans le terminal: `$ brew install git`  
  3. Puis, lances `git --version` pour vérifier s'il a été correctement installé.
  4. Une fois Git est installé tu dois ajouter tes identifiants pour te identifier. Il y a deux possibilités: via HTTPS ou par SSH. Moi j'ai choisi pour HTTPS. Lorsque je veux utiliser Gitlab, il me demande mon nom d'utilisateur et mon mot de passe.

#### Clonez une copie de travail sur mon ordinateur:  
Pour faire une copie de ton repository sur ton ordinateur, tu peux télécharger ou cloner le repository. Nous clonons le repository parce qu'il est impossible de synchroniser une version téléchargée avec Gitlab. Pour cloner le repository, tu télécharges une copie des fichiers sur ton ordinateur. Tu gardes la connexion avec Gitlab. Tu peux donc travailler sur les fichiers sur votre ordinateur et ensuite télécharger les modifications sur Gitlab.  
  1. Pour commencer, ouvres une fenêtre de terminal dans le dossier de votre ordinateur où tu souhaites enregistrer les fichiers du repository. Pour faire cela, tu tapez `cd` dans le terminal (ceci signifie 'change directory'). Puis tu peux naviguer vers le bon fichier où tu veux enregistrer le clone. Moi je garde mon clone sur mon ordinateur dans le dossier 'architecture et design'(voir photo 3).
  2. Ensuite, sur Gitlab, copies les commandes des clones. Comme je travaille avec le HTTPS, j'ai copié la commande de HTTPS (voir photo 4).  
  3. Puis, tu écris `git clone` + les commandes des clones que tu viens de trouver sur Gitlab (voir étape précédente)(voir photo 5).  
  Pendant le processus de clonage, un dossier `.git` a été automatiquement crée dans mon cas.  
  4. Pour la dernière étape, je ouvre  le clone que j'ai mis sur mon ordinateur dans Atom (voir photo 6).  

![](../images/terminalClone1.jpg)  
(photo 3)  

![](../images/clone.jpg)  
(photo 4)

![](../images/terminalClone2.jpg)  
(photo 5)  

![](../images/cloneInAtom.jpg)  
(photo 6)  

#### Travailler sur mon ordinateur et envoyer mes changement au Gitlab  
Maintenant, je peux écrire en Atom. Dans la colonne de gauche, je peux voir mon projet avec tous mes fichiers. Si je veux voir à quoi ressemblera mon texte écrit sur ma page web, je peux aller dans ‘packages’ > ‘markdown preview’ > ’toggle preview' (voir photo 7). C'est plus facile de voir ce que je fais pendant que j'écris.  

Ce que je fais pour envoyer mes modifications à Gitlab:
  1. je clique sur 'Git' dans le coin inférieur droit. Une colonne de droite apparaît Maintenant. Dans cette colonne je vois sous 'Unstaged Changes' tous les documents que j'ai modifié (textes écrits, nouvelles photos,...).  
  2. Ensuite je clique sur 'Stage All'. Les documents modifiés passent maintenant au 'Staged Changes'.  
  3. Je peux écrire chez 'Commit message' ce que j'ai modifié (voir photo 8).  
  4. Après je clique sur 'Commit to master' et finalement je clique sur 'push', tout en bas de la barre (voir photo 9). De cette façon, tous les fichiers modifiés sont téléchargés sur Gitlab.  

Pour voir ma page web en ligne, je vais sur Gitlab à 'settings' > 'pages' et là je peux trouver le lien vers ma page web (voir photo 10).

![](../images/markdownPreview.jpg)  
(photo 7)  

![](../images/stageAll.jpg)   
(photo 8)  

![](../images/push.png)  
(photo 9)  

![](../images/linkSite.jpg)  
(photo 10)  

J'ai suivi [ce tutoriel](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#install-git) en effectuant toutes les étapes précédentes.
