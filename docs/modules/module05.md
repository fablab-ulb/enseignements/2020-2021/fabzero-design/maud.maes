# 5. Usinage assisté par ordinateur  

Dans le module 5, je résumerai ce que nous avons appris pendant la formation de la Shaper Origin. Pour plus d'informations, je vous renvoie à leur [site web](https://www.shapertools.com/fr-fr/tutorials) où vous trouverez plusieurs vidéos expliquant clairement comment utiliser le Shaper.

## Shaper Origin, c'est quoi?
La Shaper Origin est un routeur portable assisté par ordinateur. Elle fonctionne avec précision, flexibilité et efficacité. Une fois que vous savez comment utiliser la machine, il est facile de démarrer, mais il est important que vous sachiez ce que vous faites pour votre propre sécurité et pour ne pas endommager la Shaper.  

Lorsque vous ouvrez la boîte, vous trouverez à côté du Shaper, également de la Shaper tape (2), une clé T-Hexagonale (3) pour le retrait de la broche, trois différents fraises (4), une clé de 16 mm (5) pour le Collet et un tuyau adaptateur (6) à brancher sur un aspirateur.  

![](../images/boiteShaper.png)  

![](../images/shaperFrontSide.jpg)  

Spécifications:  
- Ecran tactile  
- connecter par wifi où USB  
- Le profondeur de découpe max: 43 mm  
- Diamètre du collet: 8 mm  

Pour plus d'informations, voir [la fiche technique](https://www.shapertools.com/fr-fr/origin/spec) et le [guide d'utilisation](https://assets.shapertools.com/manual/Shaper_Origin_Product_Manual.pdf).  

## Comment l'utiliser?  
#### Préparer un dessin et un fichier
Pour pouvoir ouvrir un fichier sur la Shaper, il est important qu'il s'agisse d'un fichier vectoriel et qu'il soit enregistré en SVG. Ensuite, ce fichier peut être ouvert sur la shaper via wifi ou une clé usb, qui peut être insérée dans la Shaper par le côté.  

#### Préparation de la surface de travail  
Tout d'abord, le matériau à couper doit être solidement fixé à la surface de travail afin qu'il ne bouge pas lors du fraisage avec le Shaper. Vous pouvez le faire en utilisant du ruban adhésif double face, mais assurez-vous que le ruban colle suffisamment pour que le matériau ne bouge plus. Vous pouvez également visser le matériel à l'aide de vis et d'une perceuse ou le fixez avec des serre-joints. Il est recommandé d'utiliser un plateau en dessous de votre matériel de sorte que si vous asseyiez à travers votre matériau, la Shaper continuerait à fraiser sans être endommagée et sans abîmer la plan de travail.  
![](../images/planDeTravail.png)  
![](../images/ShaperdubbelzijdigeTape.jpg)  
adhésif double face pour fixer le matériau  

#### Scan de la surface de travail  
Une fois que le matériau a été fixé sur la surface de travail, on peut alors y coller le ShaperTape. La machine utilise le ShaperTape pour définir la pièce sur laquelle vous travaillez. Veillez à ce que le Shapertape soit placée dans la zone du champ de vision de la caméra de la Shaper ainsi que dans la zone de coupe (ce dernier n'est pas obligatoire). La Shaper ne peut pas fonctionner correctement s'il n'y a pas suffisamment de Shaper Tape dans le champ de vision de la caméra. Inclure au minimum deux bandes de ShaperTape dans le champ de vision.    

![](../images/shapertape2.jpg)

![](../images/shapertape11.jpg)

Pour scanner le ShaperTape, cliquez sur 'nouveau scan' dans le coin supérieur gauche de l'écran. Ensuite, vous passez avec la Shaper sur votre plan de travail jusqu'à ce que tous les blocs de dominos soient bleus. Cela signifie que le Shaper a scanné le ShaperTape. Lorsque vous avez terminé le scan, cliquez sur le bouton vert de la poignée. Si vous constatez par la suit que vous n'avez pas assez de ShaperTape, vous pouvez toujours ajouter du tape supplémentaire. Cliquez ensuite sur "Ajouter au scan".  
![](../images/shapertape3.jpg)  

#### Ouvrir et placer le dessin  
Ensuite, nous allons importer le dessin que nous voulons découper. Pour ce faire, cliquez sur le bouton 'dessiner' à droite et puis sur 'importer' à gauche. Tous les fichiers sur votre clé usb apparaissent maintenant à l'écran. Sélectionnez le bon fichier. Ensuite, déplacez la Shaper à la position souhaitée du dessin. Lorsque vous êtes satisfait du positionnement, cliquez sur le bouton vert "placer". Veillez à ce que le petit bloc de dominos en haut à droite ne devienne pas rouge comme sur la photo. Cela signifie que la Shaper ne voit pas assez de ShaperTape.  
![](../images/shaperPlacer.jpg)  

Il est également possible de faire un dessin directement sur la Shaper. Pour cela, vous cliquez au lieu de "importer" sur "créer" dans le menu "dessiner".

#### Ajuster les paramètres  
Une fois que le dessin est correctement positionné, nous pouvons régler les paramètres de coupe. Cliquez sur le menu "Fraiser" sur le côté droit. Vous verrez maintenant 5 boutons sur le côté gauche que vous pouvez utiliser pour modifier les paramètres de coupe.  
![](../images/shaperparametres.jpg)  

###### La profondeur
Le premier menu vous permet de régler la profondeur. Cela dépendra de l'épaisseur du matériau et du nombre d'étapes que vous souhaitez effectuer pour le découper. Dans cette [vidéo](https://youtu.be/qxoSaDLJRTY), ils expliquent que vous obtenez un meilleur résultat si vous coupez en plusieurs fois. Le nombre de fois que vous devez couper dépend de l'épaisseur de la mèche que vous utilisez. La règle qu'ils appliquent est la suivante: divisez l'épaisseur de votre matériau par le diamètre de votre mèche et ceci est le résultat du nombre de pas pour couper à travers votre matériau. Par exemple: si votre matériau a une épaisseur de 12 mm et que vous utilisez une mèche de 6 mm, vous pourrez couper en deux passes. Pour ce faire, réglez la profondeur à 6 mm la première fois et à 12 mm la deuxième fois.  
![](../images/shaperPasses.jpg)   
Pour changer les mèches, débranchez
d’abord le mandrin SM1 de la Shaper et retirez
le mandrin de son support. Appuyez sur la tige de verrouillage tout en desserrant ou en serrant l’écrou de verrouillage à l’aide de la clé de 19 mm fournie. Ne serrez pas l’écrou de verrouillage tant que la mèche n’est pas installée.  
![](../images/shaperChangerMeche2.png)  

![](../images/shaperChangerMeche.png)  

![](../images/shaperChangerMeche3.jpg)  

###### Position de la coupe
Dans le deuxième menu, vous pouvez choisir où exactement la mèche va couper. Cela peut être à l'intérieur de la ligne, à l'extérieur de la ligne, sur la ligne ou vous pouvez découper une surface avec l'option "poche". Cette [vidéo](https://youtu.be/B0O-0ejP-TQ) explique plus en détail ces paramètres.  
![](../images/shaperInstellingen.jpg)  

###### Offset
Le troisième menu vous donne la possibilité de fixer un décalage si nécessaire. Cette [vidéo](https://youtu.be/ESnE2bEvMg0) explique plus en détail ces paramètres.  
![](../images/shaperOffset.jpg)  

###### diamètre de la mèche
Le quatrième menu consiste à ajuster le diamètre de la mèche. Par exemple, si vous utilisez la mèche de 6 mm, vous réglez 6 mm dans ce menu.  

###### Z touch
Si vous changez de mèche, la machine vous proposera automatiquement de se recalibrer. Il le fait en exécutant la "Z touche". Cela implique que la mèche soit abaissé très doucement jusqu'à ce qu'elle touche la surface. De cette façon, la Shaper est calibré. Vous pouvez également lancer ce processus manuellement en cliquant sur le bouton "Z touch".

###### La vitesse
La vitesse de la Shaper peut être ajustée dans le dernier menu et sur la roue à l'avant de la Shaper. Dans cette [vidéo](https://youtu.be/MEk43U8mf7g), ils expliquent quelle vitesse est la plus appropriée.  
![](../images/shaperSpeed.png)  

![](../images/shaperSpeed.jpg)  

#### Couper  
Si tous les paramètres sont corrects, vous pouvez commencer le fraisage. Pour le faire en toute sécurité, il est recommandé de ne pas avoir les cheveux en vrac, de ne pas porter de vêtements amples, de ne pas porter de bijoux afin que rien ne puisse se coincer dans la machine. Portez des lunettes de protection et protégez vos oreilles du bruit.  
Allumez l'aspirateur avant de commencer à couper. Ensuite mettez le commutateur de mandrin en position marche lorsque vous êtes prêt à commencer le découpage. Cliquez ensuite sur le bouton vert situé sur la poignée de droite. le mèche va maintenant s'enfoncer à la profondeur fixée. Maintenant, vous pouvez commencer à couper. Pour ce faire, déplacez la Shaper sur votre surface de travail dans la direction indiquée à l'écran. Lorsque vous avez terminé, cliquez sur le bouton orange situé sur la poignée gauche et éteignez le mandrin.  
Lorsque vous coupez, assurez-vous que la caméra de la Shaper peut voir suffisamment de ShaperTape à tout moment. Si ce n'est plus le cas, tournez progressivement la Shaper pendant le processus de coupe afin que la caméra soit à nouveau focalisée sur le Shapertape. Si la Shaper ne voit pas assez de tape, elle arrêtera de couper.  
![](../images/shaper.jpg)  
